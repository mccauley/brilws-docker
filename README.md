# brilws-docker

## Tools from the BRIL work suite in a Docker container

## To use the image
Pull the image from the CERN GitLab container registry and start it up interactively:
```
docker run -it --name brilws gitlab-registry.cern.ch/cms-cloud/brilws-docker
```

This should bring you to a command line prompt. Then you can test `brilcalc`:

```
bril@48c3625738ff:~$ brilcalc --version
3.6.6
```

If you exit you can start up the container again with the command:

```
docker start -i brilws
```

More information on how to run `brilcalc` and what it does can be found [here](http://opendata.cern.ch/docs/cms-guide-luminosity-calculation) and [here](https://cms-opendata-workshop.github.io/workshop-lesson-luminosity/).

## To build the image
Clone this repo:

```
git clone https://gitlab.cern.ch/cms-cloud/brilws-docker.git
```

In the `brilws-docker` directory build the image:

```
cd brilws-docker
docker build -t brilws .
```

You may run locally with the following command:

```
docker run -it --rm brilws:latest
```




